from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('instagram/', views.instagram, name='instagram'),
    path('story1/', views.story1, name='story1'),
    # dilanjutkan ...
]
