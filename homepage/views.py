from django.shortcuts import render

# Create your views here.


def index(request):
    return render(request, 'index.html')


def instagram(request):
    return render(request, 'instagram.html')


def story1(request):
    return render(request, 'story1.html')
